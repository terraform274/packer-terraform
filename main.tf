module "server" {
  source = "./modules/"

  vpc_cidr = var.vpc_cidr
  cidr_public_subnet_1 = var.cidr_public_subnet_1
  cidr_public_subnet_2 = var.cidr_public_subnet_2
  availability_zone_1 = var.availability_zone_1
  availability_zone_2 = var.availability_zone_2
  cidr_private_subnet_1 = var.cidr_private_subnet_1
  cidr_private_subnet_2 = var.cidr_private_subnet_2
  public_securityGroup_name = var.public_securityGroup_name
  private_securityGroup_name = var.private_securityGroup_name
  ami = data.aws_ami.example.id
  instance_type = var.instance_type

}

data "aws_ami" "example" {
  most_recent      = true
 
  owners           = ["self"]

  filter {
    name   = "tag:Release"
    values = ["packer"]
  }

}

output "ami_id" {
    value = data.aws_ami.example.id
}
variable "vpc_cidr" {
  description = "Enter vpc cidr"
  type        = string
  default     = null
}

variable "cidr_public_subnet_1" {
  description = "Enter cidr of public subnet of first availability zone"
  type        = string
  default     = null
}

variable "cidr_public_subnet_2" {
  description = "Enter cidr of public subnet of second availability zone"
  type        = string
  default     = null
}

variable "availability_zone_1" {
  description = "Enter first availability zone"
  type        = string
  default     = "us-east-1a"
}

variable "availability_zone_2" {
  description = "Enter second availability zone"
  type        = string
  default     = "us-east-1b"
}

variable "cidr_private_subnet_1" {
  description = "Enter cidr of private subnet of first availability zone"
  type        = string
  default     = null
}

variable "cidr_private_subnet_2" {
  description = "Enter cidr of private subnet of second availability zone"
  type        = string
  default     = null
}

variable "public_securityGroup_name" {
  description = "Enter public security group name"
  type        = string
  default     = "public-sg"
}

variable "private_securityGroup_name" {
  description = "Enter private security group name"
  type        = string
  default     = "private-sg"
}

variable "ami" {
  description = "Enter Instance Ami ID"
  type        = string
  default     = "ami-0c94855ba95c71c99"
}

variable "instance_type" {
  description = "Enter Instance Type"
  type        = string
  default     = "t2.micro"
}

variable "tags_vpc" {
  description = "A mapping of tags for vpc"
  type        = map(string)
  default     = {
    Name = "vpc"
  }
}

variable "tags_internet_gateway" {
  description = "A mapping of tags for internet gateway"
  type        = map(string)
  default     = {
    Name = "internet_gateway"
  }
}

variable "tags_public_subnet_1" {
  description = "A mapping of tags for first public subnet"
  type        = map(string)
  default     = {
    Name = "public-subnet-1"
  }
}

variable "tags_public_subnet_2" {
  description = "A mapping of tags for second public subnet"
  type        = map(string)
  default     = {
    Name = "public-subnet-2"
  }
}

variable "tags_private_subnet_1" {
  description = "A mapping of tags for first private subnet"
  type        = map(string)
  default     = {
    Name = "private-subnet-1"
  }
}

variable "tags_private_subnet_2" {
  description = "A mapping of tags for second private subnet"
  type        = map(string)
  default     = {
    Name = "private-subnet-2"
  }
}

variable "tags_public_route_table" {
  description = "A mapping of tags for public route table"
  type        = map(string)
  default     = {
    Name = "public route table"
  }
}

variable "tags_private_route_table" {
  description = "A mapping of tags for private route table"
  type        = map(string)
  default     = {
    Name = "private route table"
  }
}

variable "tags_public_instance_1" {
  description = "A mapping of tags for public instance for first availablity zone"
  type        = map(string)
  default     = {
    Name = "SSH Bastion_Server_1"
  }
}

variable "tags_public_instance_2" {
  description = "A mapping of tags for public instance for second availablity zone"
  type        = map(string)
  default     = {
    Name = "SSH Bastion_Server_2"
  }
}

variable "tags_private_instance_1" {
  description = "A mapping of tags for first private instance for first availablity zone"
  type        = map(string)
  default     = {
    Name = "Auth_SVC_1"
  }
}

variable "tags_private_instance_2" {
  description = "A mapping of tags for Second private instance for first availablity zone"
  type        = map(string)
  default     = {
    Name = "Client_1"
  }
}

variable "tags_private_instance_3" {
  description = "A mapping of tags for Third private instance for first availablity zone"
  type        = map(string)
  default     = {
    Name = "Rails_DB_1"
  }
}

variable "tags_private_instance_4" {
  description = "A mapping of tags for first private instance for Second availablity zone"
  type        = map(string)
  default     = {
    Name = "Auth_SVC_2"
  }
}

variable "tags_private_instance_5" {
  description = "A mapping of tags for Second private instance for Second availablity zone"
  type        = map(string)
  default     = {
    Name = "Client_2"
  }
}

variable "tags_private_instance_6" {
  description = "A mapping of tags for Third private instance for Second availablity zone"
  type        = map(string)
  default     = {
    Name = "Rails_DB_2"
  }
}
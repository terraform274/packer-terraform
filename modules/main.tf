# VPC configuration
resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr

  tags = var.tags_vpc
}

# Internet Gateway Configuration
resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id

  tags = var.tags_internet_gateway
}

# Public subnet configuration (1)
resource "aws_subnet" "public_1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.cidr_public_subnet_1
  availability_zone = var.availability_zone_1

  tags = var.tags_public_subnet_1
}

# Public subnet configuration (2)
resource "aws_subnet" "public_2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.cidr_public_subnet_2
  availability_zone = var.availability_zone_2

  tags = var.tags_public_subnet_2
}

# Private subnet configuration (1)
resource "aws_subnet" "private_1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.cidr_private_subnet_1
  availability_zone = var.availability_zone_1

  tags = var.tags_private_subnet_1
}

# Private subnet configuration (2)
resource "aws_subnet" "private_2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.cidr_private_subnet_2
  availability_zone = var.availability_zone_2

  tags = var.tags_private_subnet_2
}

# Public Route Table Configuration
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example.id
  }

  tags = var.tags_public_route_table
}

# Private Route Table Configuration

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.example.id

  tags = var.tags_private_route_table
}

resource "aws_route_table_association" "public1" {
  subnet_id = aws_subnet.public_1.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public2" {
  subnet_id = aws_subnet.public_2.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private1" {
  subnet_id = aws_subnet.private_1.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "private2" {
  subnet_id = aws_subnet.private_2.id
  route_table_id = aws_route_table.private.id
}

# Elastic IP configuration (1)
resource "aws_eip" "private_1" {
  vpc = true
  instance = aws_instance.public_instance1.id
}

# Elastic IP configuration (2)
resource "aws_eip" "private_2" {
  vpc = true
  instance = aws_instance.public_instance2.id
}

# Security group configuration (public)
resource "aws_security_group" "public" {
  name_prefix = var.public_securityGroup_name
  vpc_id      = aws_vpc.example.id

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Security group configuration (private)
resource "aws_security_group" "private" {
  name_prefix = var.private_securityGroup_name
  vpc_id      = aws_vpc.example.id

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"
    self = true
  }
}

# EC2 instance configuration (public_1)
resource "aws_instance" "public_instance1" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.public_1.id
  availability_zone      = var.availability_zone_1
  vpc_security_group_ids = [aws_security_group.public.id]

  tags = var.tags_public_instance_1
}

# EC2 instance configuration (public_2)
resource "aws_instance" "public_instance2" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.public_2.id
  availability_zone      = var.availability_zone_2
  vpc_security_group_ids = [aws_security_group.public.id]

  tags = var.tags_public_instance_2
}

# EC2 instance configuration (private_1)
resource "aws_instance" "private_1_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_1.id
  availability_zone      = var.availability_zone_1
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_1
}

# EC2 instance configuration (private_2)
resource "aws_instance" "private_2_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_1.id
  availability_zone      = var.availability_zone_1
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_2
}

# EC2 instance configuration (private_3)
resource "aws_instance" "private_3_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_1.id
  availability_zone      = var.availability_zone_1
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_3
}

# EC2 instance configuration (private_4)
resource "aws_instance" "private_4_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_2.id
  availability_zone      = var.availability_zone_2
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_4
}

# EC2 instance configuration (private_5)
resource "aws_instance" "private_5_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_2.id
  availability_zone      = var.availability_zone_2
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_5
}

# EC2 instance configuration (private_6)
resource "aws_instance" "private_6_instance" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.private_2.id
  availability_zone      = var.availability_zone_2
  vpc_security_group_ids = [aws_security_group.private.id]

  tags = var.tags_private_instance_6
}
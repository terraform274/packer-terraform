provider "aws" {
  region     = "us-east-1"
}

terraform{
    backend "s3" {
        region = "us-east-2"
        profile = "default"
        key = "terraform/terraform.tfstate"
        bucket = "smith-bucket-1234"
    }
}

